package padronelectoral;

class PersonaInexistenteException extends RuntimeException {
    public PersonaInexistenteException(){
        super("Persona Inexistente");
    }
}