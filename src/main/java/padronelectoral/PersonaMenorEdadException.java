package padronelectoral;

public class PersonaMenorEdadException extends RuntimeException {
    public PersonaMenorEdadException(){
        super("Persona Menor de Edad");
    }
}