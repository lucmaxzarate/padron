package padronelectoral;

import java.util.ArrayList;
import java.util.Collections;

public class Padron {
    private ArrayList<Persona> nomina=new ArrayList<Persona>();

    public Padron() {
        setNomina(new ArrayList<Persona>());
    }

    public ArrayList<Persona> getNomina() {
        return nomina;
    }

    public void setNomina(ArrayList<Persona> nomina) {
        this.nomina = nomina;
    }

    public void agregarPersona(Persona persona) {
        for (Persona var : nomina) {
            if (var.equals(persona)) {
                throw new PersonaExistenteException();
            }        
        }
        this.nomina.add(persona);
    }

    public void eliminarPersona(Integer dni) {
        Persona personaEncontrada = getPersona(dni);
        nomina.remove(personaEncontrada);
    }

    public Persona getPersona(Integer dni){
        Persona personaEncontrada = null;
        for (Persona var : nomina) {
            if (var.getDni().equals(dni)) {
                personaEncontrada = var;
                break;
            }
        }
        if (personaEncontrada == null) {
            throw new PersonaInexistenteException();
        }
        return personaEncontrada;
    }

    public void modificarPersona(Persona nuevaPersona) {
        Persona personaEncontrada;
        personaEncontrada = getPersona(nuevaPersona.getDni());
        nomina.remove(personaEncontrada); 
        nomina.add(nuevaPersona);               
	}
    
    public void ordenamientoApellido(){
  
        Collections.sort(nomina);
    }

   
    public void ordenamientodni(Persona o1,Persona o2){
    ordenarporDni ord=new ordenarporDni();
    Collections.sort(nomina, ord);
    }

    

}