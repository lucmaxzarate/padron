package padronelectoral;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class PadronTest {
    @Test public void comprobarNuevoPadron() {
        Padron padron = new Padron();        
        assertEquals(0,padron.getNomina().size());
    }
    @Test public void agregarUnaPersonaAlPadron() {
        Padron padron = new Padron();    
        Persona persona = new Persona(2345678,"Quinteros",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);        
        assertEquals(1,padron.getNomina().size());
    }
    @Test public void eliminarUnaPersonaDelPadron() {
        Padron padron = new Padron();    
        Persona persona = new Persona(2345678,"Quinteros",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);          
        padron.eliminarPersona(2345678);
        assertEquals(0,padron.getNomina().size());
    }
    @Test public void modificarUnaPersonaAlPadron() {
        Padron padron = new Padron();    
        Persona persona = new Persona(2345678,"Quinteros",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);

        Persona nuevaPersona = new Persona(2345678,"Oscar Quinteros",LocalDate.parse("1974-05-23"));          
        padron.modificarPersona(nuevaPersona);

        assertEquals("Oscar Quinteros",padron.getPersona(2345678).getApellido());
    }

    @Test (expected = PersonaInexistenteException.class)
    public void buscarPersonaInexistenteEnElPadron() {
        Padron padron = new Padron();    
        Persona persona = new Persona(2345678,"Quinteros",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);
        Persona personaBuscada = padron.getPersona(2345675);
    }
    @Test
    public void ordenamientoApellido(){
        ArrayList<Persona> p=new ArrayList<Persona>();
        ArrayList<Persona> actuals=new ArrayList<Persona>();
        Padron padron = new Padron();
        Persona pers1= new Persona(1234,"Zarate Lucas",LocalDate.parse("1995-05-23"));
        padron.agregarPersona(pers1);
        Persona pers2= new Persona(4321,"Romero Nelson",LocalDate.parse("1995-04-21"));
        padron.agregarPersona(pers2);
        Persona pers3= new Persona(4213,"Tapia Lautaro",LocalDate.parse("1996-05-22"));
        padron.agregarPersona(pers3);
       
        p.add(pers2);
        p.add(pers3);
        p.add(pers1);

        padron.ordenamientoApellido();

        actuals=padron.getNomina();
        
        assertArrayEquals(p.toArray(), actuals.toArray());
        //assertNotSame(p.toArray(), actuals.toArray());
    }
@Test
public void ordenamientoDni(){
    ArrayList<Persona> p=new ArrayList<Persona>();
    ArrayList<Persona> actuals=new ArrayList<Persona>();
    Padron padron = new Padron();
    Persona pers1= new Persona(1234,"Zarate Lucas",LocalDate.parse("1995-05-23"));
    padron.agregarPersona(pers1);
    Persona pers2= new Persona(4321,"Romero Nelson",LocalDate.parse("1994-04-21"));
    padron.agregarPersona(pers2);
    Persona pers3= new Persona(4213,"Tapia Lautaro",LocalDate.parse("1996-05-22"));
    padron.agregarPersona(pers3);
   
    p.add(pers1);
    p.add(pers3);
    p.add(pers2);

    padron.ordenamientodni(pers1,pers2);
    padron.ordenamientodni(pers2,pers3);
    padron.ordenamientodni(pers1,pers3);

    actuals=padron.getNomina();
    
    assertArrayEquals(p.toArray(), actuals.toArray());
   // assertNotSame(p.toArray(), actuals.toArray());
}
}