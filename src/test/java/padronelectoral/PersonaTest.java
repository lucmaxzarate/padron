package padronelectoral;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;

public class PersonaTest {
    @Test public void comprobarNuevaPersona() {
        Persona persona = new Persona(2345678,"Quinteros",LocalDate.parse("1974-05-23"));
        
        assertEquals("Quinteros",persona.getApellido());
    }
    @Test 
    public void verificarCalculoDeEdad() {
        Persona persona = new Persona(23796687,"Quinteros",LocalDate.parse("1974-05-23"));
        assertEquals(new Integer(45),persona.edad());
    }

    @Test 
    public void verificarEquals() {
        Persona persona = new Persona(23796687,"Quinteros",LocalDate.parse("1974-05-23"));
        Persona nuevaPersona = new Persona(23796687,"Quinteros",LocalDate.parse("1974-05-23"));
        assertEquals(true,persona.equals(nuevaPersona));
    }

}